## Todoay (todos for today)

[API Documentation](https://todoay.herokuapp.com/docs/index.html)

### Dev

Run app
```
docker-compose up -d
```

Run migrations
```
docker-compose exec app php vendor/bin/doctrine-migrations migrate
```

Run tests
```
docker-compose exec app composer test
```

PHP CS Fixer:
```
docker run -it --rm -w="/app" -v "$PWD":/app php:8-fpm-alpine vendor/bin/php-cs-fixer fix --config=.php_cs.dist -v --dry-run --using-cache=no
```

PHPStan:
```
docker run -it --rm -w="/app" -v "$PWD":/app php:8-fpm-alpine vendor/bin/phpstan analyse -c phpstan.neon.dist --memory-limit=-1
```

Generate API Docs
```
docker run -it --rm -w="/app" -v "$PWD":/app php:8-fpm-alpine vendor/bin/openapi --output ./public/docs/openapi.json src config
```