<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Slim\App;

require_once __DIR__ . '/../vendor/autoload.php';

$isProd = isset($_ENV['APP_ENV']) && ($_ENV['APP_ENV'] === 'prod' || $_ENV['APP_ENV'] === 'production');
$isTest = isset($_ENV['APP_ENV']) && ($_ENV['APP_ENV'] === 'test' || $_ENV['APP_ENV'] === 'testing');

// Error reporting
if ($isProd) {
    error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);
    ini_set('display_errors', '0');
} else {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
}

// Timezone
date_default_timezone_set($_ENV['APP_TIMEZONE'] ?? 'Europe/Berlin');

$containerBuilder = new ContainerBuilder();

// Settings
$settings = require __DIR__ . '/settings.php';
$settings($containerBuilder, $isProd, $isTest);

// Dependencies
$dependencies = require __DIR__ . '/dependencies.php';
$dependencies($containerBuilder);

// Build PHP-DI Container instance
$container = $containerBuilder->build();

// Create App instance
$app = $container->get(App::class);

// Register middleware
$middleware = require __DIR__ . '/middleware.php';
$middleware($app);

// Register routes
$routes = require __DIR__ . '/routes.php';
$routes($app);

return $app;
