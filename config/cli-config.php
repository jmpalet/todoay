<?php
declare(strict_types=1);

use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\Migrations\Configuration\Migration\PhpFile;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Psr\Container\ContainerInterface;
use Slim\App;

/**
 * Config for Doctrine CLI
 */

/** @var App $app */
$app = require __DIR__ . '/bootstrap.php';

/** @var ContainerInterface $container */
$container = $app->getContainer();

$entityManager = $container->get(EntityManagerInterface::class);

if (isset($argv[0]) && $argv[0] === 'vendor/bin/doctrine') {
    return ConsoleRunner::createHelperSet($entityManager);
}

$config = new PhpFile(__DIR__ . '/migrations.php');
return DependencyFactory::fromEntityManager($config, new ExistingEntityManager($entityManager));