<?php
declare(strict_types=1);

use App\Domain\Model\Task\TaskRepositoryInterface;
use App\Domain\Model\User\UserRepositoryInterface;
use App\Infrastructure\Repository\TaskRepository;
use App\Infrastructure\Repository\UserRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use DoctrineExtensions\Query\Mysql\Date;
use Psr\Container\ContainerInterface;
use Selective\BasePath\BasePathMiddleware;
use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Middleware\ErrorMiddleware;
use DI\ContainerBuilder;

return static function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([

        App::class => function (ContainerInterface $container) {
            AppFactory::setContainer($container);

            return AppFactory::create();
        },

        ErrorMiddleware::class => function (ContainerInterface $container) {
            $app = $container->get(App::class);
            $settings = $container->get('settings')['error'];

            return new ErrorMiddleware(
                $app->getCallableResolver(),
                $app->getResponseFactory(),
                (bool)$settings['display_error_details'],
                (bool)$settings['log_errors'],
                (bool)$settings['log_error_details']
            );
        },

        // Database connection
        Connection::class => function (ContainerInterface $container) {
            $config = new Configuration();
            $connectionParams = $container->get('settings')['db'];

            return DriverManager::getConnection($connectionParams, $config);
        },

        PDO::class => function (ContainerInterface $container) {
            $settings = $container->get('settings')['db'];

            $host = $settings['host'];
            $dbname = $settings['dbname'];
            $username = $settings['user'];
            $password = $settings['password'];
            $charset = $settings['charset'];
            $flags = $settings['flags'];
            $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";

            return new PDO($dsn, $username, $password, $flags);
        },

        EntityManagerInterface::class => function (ContainerInterface $container): EntityManager {
            $dbSettings = $container->get('settings')['db'];

            $config = Setup::createAnnotationMetadataConfiguration(
                $dbSettings['doctrine']['metadata_dirs'],
                $dbSettings['doctrine']['dev_mode']
            );

            $config->addCustomStringFunction('DATE', Date::class);

            $config->setMetadataDriverImpl(
                // TODO: switch to XML data mapping
                //new XmlDriver($dbSettings['doctrine'])
                new AnnotationDriver(
                    new AnnotationReader(),
                    $dbSettings['doctrine']['metadata_dirs']
                )
            );

            // TODO: set metadata cache implementation
            //$config->setMetadataCacheImpl(
            //    new FilesystemCache($doctrineSettings['cache_dir'])
            //);

            return EntityManager::create($dbSettings, $config);
        },

        BasePathMiddleware::class => function (ContainerInterface $container) {
            return new BasePathMiddleware($container->get(App::class));
        }
    ]);
};
