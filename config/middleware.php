<?php
declare(strict_types=1);

use App\Infrastructure\Controller\AuthController;
use Psr\Container\ContainerInterface;
use Selective\BasePath\BasePathMiddleware;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;
use Tuupola\Middleware\JwtAuthentication;
use Webmozart\Assert\Assert;

return static function (App $app) {
    $container = $app->getContainer();

    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    // Add the Slim built-in routing middleware
    $app->addRoutingMiddleware();

    $app->add(BasePathMiddleware::class);

    // Catch exceptions and errors
    $app->add(ErrorMiddleware::class);

    if ($container instanceof ContainerInterface) {
        Assert::isArray($container->get('settings'));
        Assert::keyExists($container->get('settings'), 'jwt');

        $app->add(new JwtAuthentication(
            array_merge($container->get('settings')['jwt'], [
                'after' => fn ($response, $arguments) => $response
                    ->withHeader('X-Token', AuthController::refreshToken($container->get('settings')['jwt'], $arguments['decoded']))
            ])
        ));
    }
};
