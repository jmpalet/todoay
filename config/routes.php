<?php

declare(strict_types=1);

use App\Infrastructure\Controller\AuthController;
use App\Infrastructure\Controller\TaskController;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

return static function (App $app) {

    // Ping
    $app->get('/ping', function (Request $request, Response $response) {
        $response->getBody()->write('pong');
        return $response;
    });

    /**
     * @OA\Info(title="Todoay API", version="0.1")
     */
    $app->group('/api', function (RouteCollectorProxyInterface $group) {

        // User management
        $group->post('/login', AuthController::class . ':login');
        $group->post('/signup', AuthController::class . ':signup');

        /**
         * @OA\SecurityScheme(
         *   securityScheme="bearerAuth",
         *   type="http",
         *   scheme="bearer",
         *   bearerFormat="JWT"
         * )
         */
        $group->group('/v1', function (RouteCollectorProxyInterface $group) {

            // All tasks for a specific date
            $group->get('/tasks', TaskController::class . ':findAllForDate');

            // CRUD
            $group->post('/task', TaskController::class . ':create');
            $group->get('/task/{id}', TaskController::class . ':read');
            $group->put('/task/{id}', TaskController::class . ':update');
            $group->delete('/task/{id}', TaskController::class . ':delete');
        });
    });
};
