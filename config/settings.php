<?php

declare(strict_types=1);

use DI\ContainerBuilder;

if (!defined('APP_ROOT')) {
    define('APP_ROOT', __DIR__);
}

return static function (ContainerBuilder $containerBuilder, bool $isProd = true, bool $isTest = false) {
    $containerBuilder->addDefinitions([
        'settings' => [
            'error' => [
                'display_error_details' => $isProd ? false : true,
                'log_errors' => true,
                'log_error_details' => true,
            ],
            'jwt' => [
                'path' => '/api',
                'ignore' => $isTest ? ['/api'] : ['/api/login', '/api/signup'],
                'secret' => $_ENV['APP_JWT_SECRET'],
                'secure' => false, // $isProd ? true : false,
            ],
            'db' => [
                'driver' => $_ENV['DB_DRIVER'] ?? 'pdo_mysql',
                'host' => $_ENV['DB_HOST'],
                'dbname' => $_ENV['DB_DATABASE'],
                'user' => $_ENV['DB_USER'],
                'password' => $_ENV['DB_PASSWORD'],
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'flags' => [
                    // Turn off persistent connections
                    PDO::ATTR_PERSISTENT => false,
                    // Enable exceptions
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    // Emulate prepared statements
                    PDO::ATTR_EMULATE_PREPARES => true,
                    // Set default fetch mode to array
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    // Set character set
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci'
                ],
                'doctrine' => [
                    'dev_mode' => $isProd ? false : true,
                    //'cache_dir' => __DIR__.'/../var/cache/doctrine',
                    'metadata_dirs' => [APP_ROOT . '/../src/Domain/Model'],
                ]
            ],
            // TODO: commands
//            'commands' => [
//                \App\Infrastructure\Command\ConsoleCommand::class,
//            ]
        ],
    ]);
};
