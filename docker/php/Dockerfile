FROM php:8-fpm-alpine AS php8min
WORKDIR /app
RUN docker-php-ext-install -j$(nproc) \
    mysqli pdo pdo_mysql \
    opcache

FROM php8min AS composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# vvv Local only
FROM composer AS xdebug
RUN apk add --no-cache build-base gcc autoconf php8-pecl-xdebug \
    && pecl install xdebug

FROM xdebug AS dev
VOLUME /app
RUN apk add --no-cache bash

## vvv ci-dev
#FROM composer AS ci-dev
#COPY . /app
#RUN composer validate --no-check-publish && \
#    composer install --no-interaction --no-progress

# vvv ci-prod
FROM composer AS ci-prod
COPY ./docker/php/php.ini.prod /usr/local/etc/php/php.ini
COPY . /app
RUN composer validate --no-check-publish && \
    # TODO: move open api dependency out of dev in composer, and split dev/prod builds
    composer install --no-interaction --no-progress && \
    php vendor/bin/openapi --output ./public/docs/openapi.json src config