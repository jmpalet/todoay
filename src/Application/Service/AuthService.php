<?php

declare(strict_types=1);

namespace App\Application\Service;

use App\Domain\Exception\UnauthorizedException;
use App\Domain\Exception\UserAlreadyExistsException;
use App\Domain\Model\User\User;
use App\Domain\Model\User\UserCredentials;
use App\Domain\Model\User\UserRepositoryInterface;
use App\Infrastructure\Repository\UserRepository;

class AuthService
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @throws UserAlreadyExistsException
     */
    public function createUser(User $user): void
    {
        $this->userRepository->createUser($user);
    }

    /**
     * @throws UnauthorizedException
     */
    public function authorize(UserCredentials $credentials): User
    {
        $user = $this->userRepository->findByCredentials($credentials);
        return $user instanceof User ? $user : throw UnauthorizedException::withEmail($credentials->getEmail());
    }
}
