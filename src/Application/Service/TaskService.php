<?php

declare(strict_types=1);

namespace App\Application\Service;

use App\Domain\Exception\TaskNotFoundException;
use App\Domain\Model\Task\Task;
use App\Domain\Model\Task\TaskId;
use App\Domain\Model\Task\TaskRepositoryInterface;
use App\Domain\Model\Task\TaskStatus;
use App\Domain\Model\User\UserId;
use App\Infrastructure\Repository\TaskRepository;
use DateTimeInterface;

class TaskService
{
    private TaskRepositoryInterface $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * @param UserId $userId
     * @param DateTimeInterface $date
     * @return array<Task>
     */
    public function findAllByUserIdAndDate(UserId $userId, DateTimeInterface $date): array
    {
        return $this->taskRepository->getAllByUserIdAndDate($userId, $date);
    }

    /**
     * @throws TaskNotFoundException
     */
    public function findTask(TaskId $taskId, UserId $userId): Task
    {
        return $this->taskRepository->getByTaskAndUserId($taskId, $userId);
    }

    public function createTask(UserId $userId, DateTimeInterface $date, string $description): Task
    {
        $task = Task::new($userId, $date, $description);
        $this->taskRepository->persist($task);
        return $task;
    }

    /**
     * @throws TaskNotFoundException
     */
    public function updateTask(
        TaskId $taskId,
        UserId $userId,
        DateTimeInterface $date,
        string $description,
        TaskStatus $status
    ): void {
        $task = $this->taskRepository->getByTaskAndUserId($taskId, $userId);
        $task->update($date, $description, $status);
        $this->taskRepository->persist($task);
    }

    /**
     * @throws TaskNotFoundException
     */
    public function removeTask(
        TaskId $taskId,
        UserId $userId
    ): void {
        $task = $this->taskRepository->getByTaskAndUserId($taskId, $userId);
        $this->taskRepository->remove($task);
    }
}
