<?php

declare(strict_types=1);

namespace App\Domain\Exception;

use App\Domain\Model\Task\TaskId;
use Exception;

class TaskNotFoundException extends Exception
{
    public static function withTaskId(TaskId $taskId): self
    {
        return new self(sprintf('Task %s not found', $taskId->asScalar()));
    }
}
