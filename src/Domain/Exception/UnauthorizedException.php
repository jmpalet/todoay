<?php

declare(strict_types=1);

namespace App\Domain\Exception;

use App\Domain\Model\User\UserEmail;
use Exception;

class UnauthorizedException extends Exception
{
    public static function withEmail(UserEmail $email): self
    {
        return new self(sprintf('Unauthorized user %s', $email->getValue()));
    }
}
