<?php

declare(strict_types=1);

namespace App\Domain\Exception;

use App\Domain\Model\User\User;
use Exception;

class UserAlreadyExistsException extends Exception
{
    public static function withUser(User $user): self
    {
        return new self(sprintf('User %s already exists', $user->getCredentials()->getEmail()->getValue()));
    }
}
