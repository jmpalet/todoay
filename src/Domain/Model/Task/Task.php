<?php

declare(strict_types=1);

namespace App\Domain\Model\Task;

use App\Domain\Model\User\UserId;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tasks")
 */
class Task
{
    /**
     * @ORM\Embedded(class="App\Domain\Model\Task\TaskId", columnPrefix=false)
     */
    private TaskId $id;

    /**
     * @ORM\Embedded(class="App\Domain\Model\User\UserId", columnPrefix="user_")
     */
    private UserId $userId;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private DateTimeInterface $date;

    /**
     * @ORM\Column(name="description")
     */
    private string $description;

    /**
     * @ORM\Embedded(class="App\Domain\Model\Task\TaskStatus", columnPrefix=false)
     */
    private TaskStatus $status;

    public function __construct(
        TaskId $id,
        UserId $user,
        DateTimeInterface $date,
        string $description,
        TaskStatus $status
    ) {
        $this->id = $id;
        $this->userId = $user;
        $this->date = $date;
        $this->description = $description;
        $this->status = $status;
    }

    public static function new(UserId $userId, DateTimeInterface $date, string $description): self
    {
        return new self(TaskId::create(), $userId, $date, $description, TaskStatus::todo());
    }

    public function update(DateTimeInterface $date, string $description, TaskStatus $status): void
    {
        $this->date = $date;
        $this->description = $description;
        $this->status = $status;
    }

    public function getId(): TaskId
    {
        return $this->id;
    }

    public function getUserId(): UserId
    {
        return $this->userId;
    }

    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getStatus(): TaskStatus
    {
        return $this->status;
    }
}
