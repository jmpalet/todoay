<?php

declare(strict_types=1);

namespace App\Domain\Model\Task;

use App\Support\Identity\UuidIdentity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class TaskId extends UuidIdentity
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid")
     */
    protected string $identity;
}
