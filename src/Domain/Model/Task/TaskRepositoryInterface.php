<?php

declare(strict_types=1);

namespace App\Domain\Model\Task;

use App\Domain\Exception\TaskNotFoundException;
use App\Domain\Model\User\UserId;
use DateTimeInterface;

interface TaskRepositoryInterface
{
    /**
     * @param UserId $userId
     * @param DateTimeInterface $date
     * @return array<Task>
     */
    public function getAllByUserIdAndDate(UserId $userId, DateTimeInterface $date): array;

    /**
     * @param TaskId $taskId
     * @param UserId $userId
     * @return Task
     * @throws TaskNotFoundException
     */
    public function getByTaskAndUserId(TaskId $taskId, UserId $userId): Task;

    public function persist(Task $task): void;

    public function remove(Task $task): void;
}
