<?php

declare(strict_types=1);

namespace App\Domain\Model\Task;

use App\Support\Identity\StringIdentity;
use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable()
 */
class TaskStatus extends StringIdentity
{
    private const STATUS_TODO = 'todo';
    private const STATUS_DONE = 'done';

    private const ALLOWED_STATUS = [
        self::STATUS_TODO,
        self::STATUS_DONE,
    ];

    /**
     * @ORM\Column(name="status")
     */
    protected string $identity;

    public static function todo(): self
    {
        return new self(self::STATUS_TODO);
    }

    public static function done(): self
    {
        return new self(self::STATUS_DONE);
    }

    public static function fromString(string $status): self
    {
        Assert::inArray($status, self::ALLOWED_STATUS);
        return new self($status);
    }
}
