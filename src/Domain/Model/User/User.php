<?php

declare(strict_types=1);

namespace App\Domain\Model\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @ORM\Embedded(class="App\Domain\Model\User\UserId", columnPrefix=false)
     */
    private UserId $id;

    /**
     * @ORM\Embedded(class="App\Domain\Model\User\UserCredentials", columnPrefix=false)
     */
    private UserCredentials $credentials;

    final protected function __construct(UserId $id, UserCredentials $credentials)
    {
        $this->id = $id;
        $this->credentials = $credentials;
    }

    public static function withCredentials(string $email, string $password): self
    {
        return new static(UserId::create(), UserCredentials::withEmailAndPassword($email, $password));
    }

    public function getId(): UserId
    {
        return $this->id;
    }

    public function getCredentials(): UserCredentials
    {
        return $this->credentials;
    }
}
