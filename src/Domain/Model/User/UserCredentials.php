<?php

declare(strict_types=1);

namespace App\Domain\Model\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class UserCredentials
{
    /**
     * @ORM\Embedded(class="App\Domain\Model\User\UserEmail", columnPrefix=false)
     */
    private UserEmail $email;

    /**
     * @ORM\Embedded(class="App\Domain\Model\User\UserPassword", columnPrefix=false)
     */
    private UserPassword $password;

    protected function __construct(UserEmail $email, UserPassword $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public static function withEmailAndPassword(string $email, string $password): self
    {
        return new self(UserEmail::fromValue($email), UserPassword::fromSecret($password));
    }

    public function getEmail(): UserEmail
    {
        return $this->email;
    }

    public function getPassword(): UserPassword
    {
        return $this->password;
    }
}
