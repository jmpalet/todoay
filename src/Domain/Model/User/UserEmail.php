<?php

declare(strict_types=1);

namespace App\Domain\Model\User;

use App\Domain\Model\ValueObject\Email;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class UserEmail extends Email
{
    /**
     * @ORM\Column(name="email")
     */
    protected string $value;
}
