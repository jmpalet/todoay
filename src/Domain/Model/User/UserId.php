<?php

declare(strict_types=1);

namespace App\Domain\Model\User;

use App\Support\Identity\UuidIdentity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class UserId extends UuidIdentity
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid")
     */
    protected string $identity;
}
