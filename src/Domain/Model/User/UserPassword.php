<?php

declare(strict_types=1);

namespace App\Domain\Model\User;

use App\Domain\Model\ValueObject\Password;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class UserPassword extends Password
{
    /**
     * @ORM\Column(name="password")
     */
    protected false|null|string $hash;
}
