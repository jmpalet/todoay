<?php

declare(strict_types=1);

namespace App\Domain\Model\User;

use App\Domain\Exception\UserAlreadyExistsException;

interface UserRepositoryInterface
{
    /**
     * @param User $user
     * @throws UserAlreadyExistsException
     */
    public function createUser(User $user): void;

    public function findByCredentials(UserCredentials $credentials): ?User;
}
