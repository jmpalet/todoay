<?php

declare(strict_types=1);

namespace App\Domain\Model\ValueObject;

use Webmozart\Assert\Assert;

class Email
{
    protected string $value;

    final protected function __construct(string $value)
    {
        $this->value = $value;
    }

    public static function fromValue(string $value): static
    {
        Assert::email($value);
        return new static($value);
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
