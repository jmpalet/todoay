<?php

declare(strict_types=1);

namespace App\Domain\Model\ValueObject;

class Password
{
    protected false|null|string $hash;
    private string $secret;

    final protected function __construct(string $secret)
    {
        $this->secret = $secret;
        $this->hash = password_hash($secret, PASSWORD_DEFAULT);
    }

    private function getSecret(): string
    {
        return $this->secret;
    }

    public static function fromSecret(string $secret): static
    {
        return new static($secret);
    }

    public function equals(self $other): bool
    {
        return password_verify($other->getSecret(), $this->hash);
    }
}
