<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Application\Service\AuthService;
use App\Domain\Exception\UnauthorizedException;
use App\Domain\Exception\UserAlreadyExistsException;
use App\Domain\Model\User\User;
use App\Domain\Model\User\UserCredentials;
use App\Infrastructure\View\Builder\AuthResponseBuilder;
use Fig\Http\Message\StatusCodeInterface;
use Firebase\JWT\JWT;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Psr7\Request;
use Webmozart\Assert\Assert;

class AuthController
{
    public const JWT_TOKEN_EXP_TIME = 15 * 60; // 15 min

    private ContainerInterface $container;
    private AuthService $authService;
    private AuthResponseBuilder $responseBuilder;

    public function __construct(
        ContainerInterface $container,
        AuthService $authService,
        AuthResponseBuilder $responseBuilder
    ) {
        $this->container = $container;
        $this->authService = $authService;
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * @OA\Post(
     *   tags={"User"},
     *   path="/api/signup",
     *   operationId="signup",
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *    	 @OA\Schema(
     *    	   @OA\Property(property="email",
     *    	     type="string",
     *    		 example="user@example.com",
     *    	   ),
     *    	   @OA\Property(property="password",
     *           type="string",
     *           example="myS3cureP4ssword"
     *    	   ),
     *    	 ),
     *     ),
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="User created correctly"
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Bad request",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="409",
     *     description="User already exists",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="Internal Server Error",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   )
     * )
     */
    public function signup(Request $request, Response $response): Response
    {
        $body = $request->getParsedBody();

        try {
            Assert::keyExists($body, 'email');
            Assert::keyExists($body, 'password');

            $user = User::withCredentials($body['email'], $body['password']);
        } catch (InvalidArgumentException $exception) {
            return $this->responseBuilder->buildErrorResponse($response, $exception);
        }

        try {
            $this->authService->createUser($user);

            // OK
            return $response->withStatus(StatusCodeInterface::STATUS_OK);
        } catch (UserAlreadyExistsException $exception) {
            return $this->responseBuilder->buildErrorResponse($response, $exception);
        }
    }

    /**
     * @OA\Post(
     *   tags={"User"},
     *   path="/api/login",
     *   operationId="login",
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *    	 @OA\Schema(
     *    	   @OA\Property(property="email",
     *    	     type="string",
     *    		 example="user@example.com",
     *    	   ),
     *    	   @OA\Property(property="password",
     *           type="string",
     *           example="myS3cureP4ssword"
     *    	   ),
     *    	 ),
     *     ),
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Successful login",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/AuthResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Bad request",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="Internal Server Error",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   )
     * )
     */
    public function login(Request $request, Response $response): Response
    {
        $body = $request->getParsedBody();

        try {
            Assert::keyExists($body, 'email');
            Assert::keyExists($body, 'password');
            $userCredentials = UserCredentials::withEmailAndPassword($body['email'], $body['password']);
        } catch (InvalidArgumentException $exception) {
            return $this->responseBuilder->buildErrorResponse($response, $exception);
        }

        try {
            $user = $this->authService->authorize($userCredentials);
        } catch (UnauthorizedException $exception) {
            return $this->responseBuilder->buildErrorResponse($response, $exception);
        }

        $settings = $this->container->get('settings');
        $token = self::createToken($settings['jwt'], self::createTokenParams($user));
        return $this->responseBuilder->buildTokenResponse($response, $token);
    }

    /**
     * @param array<mixed> $jwtSettings
     * @param array<mixed> $params
     * @return string
     */
    public static function refreshToken(array $jwtSettings, array $params): string
    {
        Assert::keyExists($params, 'id');
        Assert::keyExists($params, 'email');
        Assert::keyExists($params, 'iat');
        Assert::keyExists($params, 'exp');
        $params['exp'] = time() + self::JWT_TOKEN_EXP_TIME;
        return self::createToken($jwtSettings, $params);
    }

    /**
     * @param array<mixed> $jwtSettings
     * @param array<mixed> $params
     * @return string
     */
    public static function createToken(array $jwtSettings, array $params): string
    {
        return JWT::encode([
            'id' => $params['id'],
            'email' => $params['email'],
            'iat' => $params['iat'],
            'exp' => $params['exp'],
        ], $jwtSettings['secret']);
    }

    /**
     * @param User $user
     * @return array<mixed>
     */
    public static function createTokenParams(User $user): array
    {
        return [
            'id' => $user->getId()->asScalar(),
            'email' => $user->getCredentials()->getEmail()->getValue(),
            'iat' => time(),
            'exp' => time() + self::JWT_TOKEN_EXP_TIME,
        ];
    }
}
