<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Application\Service\TaskService;
use App\Domain\Exception\TaskNotFoundException;
use App\Domain\Model\Task\TaskId;
use App\Domain\Model\Task\TaskStatus;
use App\Domain\Model\User\UserId;
use App\Infrastructure\View\Builder\TaskResponseBuilder;
use DateTime;
use Exception;
use Fig\Http\Message\StatusCodeInterface;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Psr7\Request;
use Webmozart\Assert\Assert;

class TaskController
{
    private TaskService $taskService;
    private TaskResponseBuilder $taskResponseBuilder;

    public function __construct(TaskService $taskService, TaskResponseBuilder $taskResponseBuilder)
    {
        $this->taskService = $taskService;
        $this->taskResponseBuilder = $taskResponseBuilder;
    }

    /**
     * @OA\Get(
     *   tags={"Task"},
     *   path="/api/v1/tasks",
     *   operationId="find",
     *   @OA\Parameter(
     *     in="query",
     *     name="date",
     *     example="2021-04-01T10:00:00+00:00",
     *     @OA\Schema(
     *       type="string"
     *     ),
     *     required=true,
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="List all tasks by user and date",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/TaskResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Bad request",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="Internal Server Error",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   )
     * )
     */
    public function findAllForDate(Request $request, Response $response): Response
    {
        $token = $request->getAttribute('token');
        $params = $request->getQueryParams();

        try {
            Assert::keyExists($token, 'id');
            Assert::keyExists($params, 'date');
            $userId = UserId::fromIdentity($token['id']);
            $date = new DateTime($params['date']);
        } catch (Exception $exception) {
            return $this->taskResponseBuilder->buildErrorResponse($response, $exception);
        }

        return $this->taskResponseBuilder->buildArrayResponse($response, $this->taskService->findAllByUserIdAndDate($userId, $date));
    }

    /**
     * @OA\Post(
     *   tags={"Task"},
     *   path="/api/v1/task",
     *   operationId="create",
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *    	 @OA\Schema(
     *    	   @OA\Property(property="description",
     *    	     type="string",
     *    		 example="My task",
     *    	   ),
     *    	   @OA\Property(property="date",
     *           type="string",
     *           example="2021-04-01T10:00:00+00:00"
     *    	   ),
     *    	 ),
     *     ),
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Created task object",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/Task"
     *     )
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Bad request",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="Internal Server Error",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   )
     * )
     */
    public function create(Request $request, Response $response): Response
    {
        $body = $request->getParsedBody();
        $token = $request->getAttribute('token');

        try {
            Assert::keyExists($body, 'description');
            Assert::keyExists($body, 'date');
            Assert::keyExists($token, 'id');
            $userId = UserId::fromIdentity($token['id']);
            $date = new DateTime($body['date']);
            $body = $body['description'];
        } catch (InvalidARgumentException|Exception $exception) {
            return $this->taskResponseBuilder->buildErrorResponse($response, $exception);
        }

        try {
            $task = $this->taskService->createTask($userId, $date, $body);
            return $this->taskResponseBuilder->buildSingleResponse($response, $task);
        } catch (Exception $exception) {
            return $this->taskResponseBuilder->buildErrorResponse($response, $exception);
        }
    }

    /**
     * @OA\Get(
     *   tags={"Task"},
     *   path="/api/v1/task/{taskId}",
     *   operationId="read",
     *   @OA\Parameter(
     *     in="path",
     *     name="taskId",
     *     example="9dbbd295-e0a4-4860-a4e5-f076f44c4162",
     *     @OA\Schema(
     *       type="string"
     *     ),
     *     required=true,
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Task object",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/Task"
     *     )
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Bad request",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="404",
     *     description="Not found",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="Internal Server Error",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   )
     * )
     *
     * @param array<mixed> $args
     */
    public function read(Request $request, Response $response, array $args): Response
    {
        $token = $request->getAttribute('token');

        try {
            Assert::keyExists($token, 'id');
            Assert::keyExists($args, 'id');
            $taskId = TaskId::fromIdentity($args['id']);
            $userId = UserId::fromIdentity($token['id']);
        } catch (InvalidARgumentException $exception) {
            return $this->taskResponseBuilder->buildErrorResponse($response, $exception);
        }

        try {
            $task = $this->taskService->findTask($taskId, $userId);

            return $this->taskResponseBuilder->buildSingleResponse($response, $task);
        } catch (TaskNotFoundException $exception) {
            return $this->taskResponseBuilder->buildErrorResponse($response, $exception);
        }
    }

    /**
     * @OA\Put(
     *   tags={"Task"},
     *   path="/api/v1/task/{taskId}",
     *   operationId="update",
     *   @OA\Parameter(
     *     in="path",
     *     name="taskId",
     *     @OA\Schema(type="string", example="9dbbd295-e0a4-4860-a4e5-f076f44c4162"),
     *     required=true
     *   ),
     *   @OA\RequestBody(
     *     @OA\MediaType(
     *       mediaType="application/json",
     *    	 @OA\Schema(
     *    	   @OA\Property(property="description",
     *             type="string",
     *             example="My task",
     *           ),
     *    	   @OA\Property(property="date",
     *           type="string",
     *           example="2021-04-01T10:00:00+00:00",
     *           ),
     *         @OA\Property(property="status",
     *             type="string",
     *           enum={"done", "todo"},
     *             example="done",
     *           ),
     *         ),
     *     ),
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Task updated"
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Bad request",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="404",
     *     description="Not found",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="Internal Server Error",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   )
     * )
     * @param array<mixed> $args
     */
    public function update(Request $request, Response $response, array $args): Response
    {
        $body = $request->getParsedBody();
        $token = $request->getAttribute('token');

        try {
            Assert::keyExists($token, 'id');
            Assert::keyExists($args, 'id');
            Assert::keyExists($body, 'description');
            Assert::keyExists($body, 'date');
            Assert::keyExists($body, 'status');
            $taskId = TaskId::fromIdentity($args['id']);
            $userId = UserId::fromIdentity($token['id']);
            $taskStatus = TaskStatus::fromString($body['status']);
            $date = new DateTime($body['date']);
            $description = $body['description'];
        } catch (Exception $exception) {
            return $this->taskResponseBuilder->buildErrorResponse($response, $exception);
        }

        try {
            $this->taskService->updateTask($taskId, $userId, $date, $description, $taskStatus);

            // OK
            return $response->withStatus(StatusCodeInterface::STATUS_OK);
        } catch (TaskNotFoundException $exception) {
            return $this->taskResponseBuilder->buildErrorResponse($response, $exception);
        }
    }

    /**
     * @OA\Delete(
     *   tags={"Task"},
     *   path="/api/v1/task/{taskId}",
     *   operationId="delete",
     *   @OA\Parameter(
     *     in="path",
     *     name="taskId",
     *     @OA\Schema(type="string", example="9dbbd295-e0a4-4860-a4e5-f076f44c4162"),
     *     required=true
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="Task deleted",
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Bad request",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="404",
     *     description="Not found",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="Internal Server Error",
     *     @OA\JsonContent(
     *       ref="#/components/schemas/ErrorResponse"
     *     )
     *   )
     * )
     * @param array<mixed> $args
     */
    public function delete(Request $request, Response $response, array $args): Response
    {
        $token = $request->getAttribute('token');

        try {
            Assert::keyExists($token, 'id');
            Assert::keyExists($args, 'id');
            $taskId = TaskId::fromIdentity($args['id']);
            $userId = UserId::fromIdentity($token['id']);
        } catch (InvalidARgumentException $exception) {
            return $this->taskResponseBuilder->buildErrorResponse($response, $exception);
        }

        try {
            $this->taskService->removeTask($taskId, $userId);
            return $response->withStatus(StatusCodeInterface::STATUS_OK);
        } catch (TaskNotFoundException $exception) {
            return $this->taskResponseBuilder->buildErrorResponse($response, $exception);
        }
    }
}
