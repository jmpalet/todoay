<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Exception\TaskNotFoundException;
use App\Domain\Model\Task\Task;
use App\Domain\Model\Task\TaskId;
use App\Domain\Model\Task\TaskRepositoryInterface;
use App\Domain\Model\User\UserId;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;

class TaskRepository implements TaskRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function getAllByUserIdAndDate(UserId $userId, DateTimeInterface $date): array
    {
        return $this->entityManager->createQueryBuilder()
            ->select('t')
            ->from(Task::class, 't')
            ->where('t.userId.identity = :userId')
            ->andWhere('DATE(t.date) = :date')
            ->setParameter(':userId', $userId->asScalar())
            ->setParameter(':date', $date->format('Y-m-d'))
            ->getQuery()
            ->execute();
    }

    public function getByTaskAndUserId(TaskId $taskId, UserId $userId): Task
    {
        $task = $this->entityManager->getRepository(Task::class)->findOneBy([
            'id.identity' => $taskId->asScalar(),
            'userId.identity' => $userId->asScalar(),
        ]);

        return $task instanceof Task ? $task : throw TaskNotFoundException::withTaskId($taskId);
    }

    public function persist(Task $task): void
    {
        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    public function remove(Task $task): void
    {
        $this->entityManager->remove($task);
        $this->entityManager->flush();
    }
}
