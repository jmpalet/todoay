<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Exception\UserAlreadyExistsException;
use App\Domain\Model\User\User;
use App\Domain\Model\User\UserCredentials;
use App\Domain\Model\User\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class UserRepository implements UserRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createUser(User $user): void
    {
        if (!is_null($this->entityManager->getRepository(User::class)->findOneBy([
            'credentials.email.value' => $user->getCredentials()->getEmail()->getValue(),
        ]))) {
            throw UserAlreadyExistsException::withUser($user);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function findByCredentials(UserCredentials $credentials): ?User
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'credentials.email.value' => $credentials->getEmail()->getValue(),
        ]);

        if ($user instanceof User && $user->getCredentials()->getPassword()->equals($credentials->getPassword())) {
            return $user;
        }

        return null;
    }
}
