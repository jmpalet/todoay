<?php

declare(strict_types=1);

namespace App\Infrastructure\View\Builder;

use App\Domain\Exception\UnauthorizedException;
use App\Domain\Exception\UserAlreadyExistsException;
use App\Infrastructure\View\Response\AuthResponse;
use Exception;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface as Response;

class AuthResponseBuilder extends ResponseBuilder
{
    protected function setErrorStatus(Exception $exception): int
    {
        if ($exception instanceof UnauthorizedException) {
            return StatusCodeInterface::STATUS_UNAUTHORIZED;
        }

        if ($exception instanceof UserAlreadyExistsException) {
            return StatusCodeInterface::STATUS_CONFLICT;
        }

        return parent::setErrorStatus($exception);
    }

    public function buildTokenResponse(Response $response, string $token): Response
    {
        $response->getBody()->write((new AuthResponse($token))->jsonSerialize());

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
