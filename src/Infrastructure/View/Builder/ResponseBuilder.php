<?php

declare(strict_types=1);

namespace App\Infrastructure\View\Builder;

use App\Infrastructure\View\Response\ErrorResponse;
use Exception;
use Fig\Http\Message\StatusCodeInterface;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as Response;

class ResponseBuilder
{
    public function buildErrorResponse(Response $response, Exception $exception): Response
    {
        $response = $this->buildErrorBody($response, $exception);
        return $response->withStatus($this->setErrorStatus($exception), $exception->getMessage());
    }

    protected function buildErrorBody(Response $response, Exception $exception): Response
    {
        $response->getBody()->write((new ErrorResponse($exception->getMessage()))->jsonSerialize());

        return $response;
    }

    protected function setErrorStatus(Exception $exception): int
    {
        if ($exception instanceof InvalidArgumentException) {
            return StatusCodeInterface::STATUS_BAD_REQUEST;
        }

        return StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR;
    }
}
