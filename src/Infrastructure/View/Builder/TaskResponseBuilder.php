<?php

declare(strict_types=1);

namespace App\Infrastructure\View\Builder;

use App\Domain\Exception\TaskNotFoundException;
use App\Domain\Model\Task\Task as DomainModel;
use App\Infrastructure\View\Response\TaskResponse;
use App\Infrastructure\View\Transformer\TaskTransformer;
use Exception;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface as Response;
use function json_encode;

class TaskResponseBuilder extends ResponseBuilder
{
    protected function setErrorStatus(Exception $exception): int
    {
        if ($exception instanceof TaskNotFoundException) {
            return StatusCodeInterface::STATUS_NOT_FOUND;
        }

        return parent::setErrorStatus($exception);
    }

    /**
     * @param Response $response
     * @param array<DomainModel> $models
     * @return Response
     */
    public function buildArrayResponse(Response $response, array $models): Response
    {
        $response->getBody()->write((new TaskResponse($models))->jsonSerialize());

        return $response;
    }

    /**
     * @param Response $response
     * @param DomainModel $model
     * @return Response
     */
    public function buildSingleResponse(Response $response, DomainModel $model): Response
    {
        $response->getBody()->write(
            json_encode(TaskTransformer::transformOne($model)->toArray(), JSON_THROW_ON_ERROR)
        );

        return $response;
    }
}
