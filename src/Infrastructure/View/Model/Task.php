<?php

declare(strict_types=1);

namespace App\Infrastructure\View\Model;

/**
 * @OA\Schema(
 *     title="Task",
 *     description="Task view model"
 * )
 */
class Task
{
    /**
     * @OA\Property(type="string", example="9dbbd295-e0a4-4860-a4e5-f076f44c4162")
     */
    private string $id;

    /**
     * @OA\Property(type="string", example="My task")
     */
    private string $description;

    /**
     * @OA\Property(type="string", example="todo")
     */
    private string $status;

    protected function __construct(string $id, string $description, string $status)
    {
        $this->id = $id;
        $this->description = $description;
        $this->status = $status;
    }

    public static function withParams(string $id, string $description, string $status): self
    {
        return new self($id, $description, $status);
    }

    /**
     * @return array<mixed>
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'status' => $this->status,
        ];
    }
}
