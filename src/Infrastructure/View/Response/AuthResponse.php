<?php

declare(strict_types=1);

namespace App\Infrastructure\View\Response;

use JsonSerializable;

/**
 * @OA\Schema(
 *     title="AuthResponse",
 *     description="Authentication response",
 * )
 */
class AuthResponse implements JsonSerializable
{
    /**
     * @OA\Property()
     */
    private string $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    public function jsonSerialize(): string
    {
        return json_encode([
                'token' => $this->token,
            ], JSON_THROW_ON_ERROR);
    }
}
