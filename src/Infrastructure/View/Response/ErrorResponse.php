<?php

declare(strict_types=1);

namespace App\Infrastructure\View\Response;

use JsonSerializable;

/**
 * @OA\Schema(
 *     title="TaskResponse",
 *     description="Task response",
 * )
 */
class ErrorResponse implements JsonSerializable
{
    /**
     * @OA\Property()
     */
    private string $error;

    public function __construct(string $error)
    {
        $this->error = $error;
    }

    public function jsonSerialize(): string
    {
        return json_encode([
            'error' => $this->error,
        ], JSON_THROW_ON_ERROR);
    }
}
