<?php

declare(strict_types=1);

namespace App\Infrastructure\View\Response;

use App\Domain\Model\Task\Task as DomainModel;
use App\Infrastructure\View\Model\Task as ViewModel;
use App\Infrastructure\View\Transformer\TaskTransformer;
use JsonSerializable;

/**
 * @OA\Schema(
 *     title="TaskResponse",
 *     description="Task response",
 * )
 */
class TaskResponse implements JsonSerializable
{
    /**
     * @var array<DomainModel>
     * @OA\Property(
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/Task")
     * )
     */
    private array $tasks;

    /**
     * @param array<DomainModel> $tasks
     */
    public function __construct(array $tasks)
    {
        $this->tasks = $tasks;
    }

    public function jsonSerialize(): string
    {
        return json_encode([
                'tasks' => array_map(static fn (ViewModel $model) => $model->toArray(), TaskTransformer::transform(...$this->tasks)),
            ], JSON_THROW_ON_ERROR);
    }
}
