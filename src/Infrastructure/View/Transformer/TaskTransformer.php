<?php

declare(strict_types=1);

namespace App\Infrastructure\View\Transformer;

use App\Domain\Model\Task\Task as DomainModel;
use App\Infrastructure\View\Model\Task as ViewModel;

class TaskTransformer
{
    /**
     * @param DomainModel ...$models
     * @return array<ViewModel>
     */
    public static function transform(DomainModel ...$models): array
    {
        return array_map(
            static fn (DomainModel $model) => self::transformOne($model),
            $models
        );
    }

    public static function transformOne(DomainModel $model): ViewModel
    {
        return ViewModel::withParams(
            $model->getId()->asScalar(),
            $model->getDescription(),
            $model->getStatus()->asScalar()
        );
    }
}
