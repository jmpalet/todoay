<?php

declare(strict_types=1);

namespace App\Support\Identity;

abstract class AbstractIdentity implements IdentityInterface
{
    public function equal(IdentityInterface $other): bool
    {
        return static::class === get_class($other)
            && $this->asScalar() === $other->asScalar();
    }

    public function notEqual(IdentityInterface $other): bool
    {
        return !$this->equal($other);
    }
}
