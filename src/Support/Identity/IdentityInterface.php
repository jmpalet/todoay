<?php

declare(strict_types=1);

namespace App\Support\Identity;

interface IdentityInterface
{
    public function asScalar(): mixed;

    public function __toString(): string;

    public function equal(IdentityInterface $other): bool;

    public function notEqual(IdentityInterface $other): bool;
}
