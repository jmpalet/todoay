<?php

declare(strict_types=1);

namespace App\Support\Identity;

use Webmozart\Assert\Assert;

class StringIdentity extends AbstractIdentity
{
    protected string $identity;

    final protected function __construct(string $identity)
    {
        Assert::string($identity);
        Assert::notEmpty($identity);

        $this->identity = $identity;
    }

    public function __toString(): string
    {
        return $this->asScalar();
    }

    public function asScalar(): string
    {
        return (string) $this->identity;
    }
}
