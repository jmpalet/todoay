<?php

declare(strict_types=1);

namespace App\Support\Identity;

use Ramsey\Uuid\Uuid;

class UuidIdentity extends StringIdentity
{
    public static function create(): static
    {
        return new static(Uuid::uuid4()->toString());
    }

    public static function fromIdentity(string $identity): static
    {
        return new static(Uuid::fromString($identity)->toString());
    }
}
