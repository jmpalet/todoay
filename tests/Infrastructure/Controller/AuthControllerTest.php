<?php

declare(strict_types=1);

namespace App\Test\Infrastructure\Controller;

use App\Domain\Exception\UserAlreadyExistsException;
use App\Domain\Model\User\User;
use App\Infrastructure\Repository\UserRepository;
use App\Test\Traits\AppTestTrait;
use Closure;
use Fig\Http\Message\StatusCodeInterface;
use Generator;
use PHPUnit\Framework\TestCase;

class AuthControllerTest extends TestCase
{
    use AppTestTrait;

    /**
     * @dataProvider signupTestDataProvider
     * @param array<mixed> $body
     */
    public function testSignup(array $body, Closure $mock, int $status): void
    {
        // GIVEN
        $mock->call($this);

        // WHEN
        $request = $this->createJsonRequest('POST', '/api/signup', $body);
        $response = $this->app->handle($request);

        // THEN
        self::assertEquals($status, $response->getStatusCode());
    }

    /**
     * @return Generator<array>
     */
    public function signupTestDataProvider(): Generator
    {
        $email = 'test@test.com';
        $password = '12345678';
        $user = User::withCredentials($email, $password);

        yield 'valid email and password, user does not exist yet' => [
            'body' => compact('email', 'password'),
            'mock' => fn () => $this->mock(UserRepository::class)
                    ->method('createUser'),
            'status' => StatusCodeInterface::STATUS_OK,
        ];

        yield 'valid email and password, but user already exists' => [
            'body' => compact('email', 'password'),
            'mock' => fn () => $this->mock(UserRepository::class)
                    ->method('createUser')
                    ->willThrowException(UserAlreadyExistsException::withUser($user)),
            'status' => StatusCodeInterface::STATUS_CONFLICT,
        ];

        yield 'invalid email throws bad request exception' => [
            'body' => ['email' => 'test', 'password' => $password],
            'mock' => fn () => null,
            'status' => StatusCodeInterface::STATUS_BAD_REQUEST,
        ];

        yield 'missing field, bad request exception' => [
            'body' => compact('email'),
            'mock' => fn () => null,
            'status' => StatusCodeInterface::STATUS_BAD_REQUEST,
        ];
    }

    /**
     * @dataProvider loginTestDataProvider
     * @param array<mixed> $body
     */
    public function testLogin(array $body, Closure $mock, int $status): void
    {
        // GIVEN
        $mock->call($this);

        // WHEN
        $request = $this->createJsonRequest('POST', '/api/login', $body);
        $response = $this->app->handle($request);

        // THEN
        self::assertEquals($status, $response->getStatusCode());
    }

    /**
     * @return Generator<array>
     */
    public function loginTestDataProvider(): Generator
    {
        $email = 'test@test.com';
        $password = '12345678';
        $user = User::withCredentials($email, $password);

        yield 'valid email and password, user exists' => [
            'body' => compact('email', 'password'),
            'mock' => fn () => $this->mock(UserRepository::class)
                ->method('findByCredentials')
                ->willReturn($user),
            'status' => StatusCodeInterface::STATUS_OK,
        ];

        yield 'valid email and password, but user does not exist' => [
            'body' => compact('email', 'password'),
            'mock' => fn () => $this->mock(UserRepository::class)
                ->method('findByCredentials')
                ->willReturn(null),
            'status' => StatusCodeInterface::STATUS_UNAUTHORIZED,
        ];

        yield 'invalid email throws bad request exception' => [
            'body' => ['email' => 'test', 'password' => $password],
            'mock' => fn () => null,
            'status' => StatusCodeInterface::STATUS_BAD_REQUEST,
        ];

        yield 'missing field, bad request exception' => [
            'body' => compact('email'),
            'mock' => fn () => null,
            'status' => StatusCodeInterface::STATUS_BAD_REQUEST,
        ];
    }
}
