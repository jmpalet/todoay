<?php

declare(strict_types=1);

namespace App\Test\Infrastructure\Controller;

use App\Test\Traits\AppTestTrait;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\TestCase;

class PingTest extends TestCase
{
    use AppTestTrait;

    public function testPing(): void
    {
        $request = $this->createRequest('GET', '/ping');
        $response = $this->app->handle($request);

        self::assertEquals(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
    }
}