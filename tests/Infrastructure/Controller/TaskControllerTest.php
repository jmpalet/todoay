<?php

declare(strict_types=1);

namespace App\Test\Infrastructure\Controller;

use App\Domain\Model\Task\Task;
use App\Domain\Model\User\User;
use App\Domain\Model\User\UserId;
use App\Infrastructure\Repository\TaskRepository;
use App\Infrastructure\View\Response\TaskResponse;
use App\Infrastructure\View\Transformer\TaskTransformer;
use App\Test\Traits\AppTestTrait;
use Closure;
use DateTime;
use Fig\Http\Message\StatusCodeInterface;
use Generator;
use JsonSerializable;
use PHPUnit\Framework\TestCase;

class TaskControllerTest extends TestCase
{
    use AppTestTrait;

    /**
     * @dataProvider findAllTestDataProvider
     */
    public function testFindAll(User $user, Closure $mock, int $status, JsonSerializable $expected): void
    {
        // GIVEN
        $mock->call($this);

        // WHEN
        $request = $this->createJsonRequest('GET', sprintf('/api/v1/tasks?date=%s', urlencode((new DateTime())->format(DATE_ATOM))));
        $request = $request->withAttribute('token', ['id' => $user->getId()->asScalar()]);
        $response = $this->app->handle($request);

        // THEN
        self::assertEquals($status, $response->getStatusCode());
        self::assertEquals($expected->jsonSerialize(), (string) $response->getBody());
    }

    /**
     * @return Generator<array>
     */
    public function findAllTestDataProvider(): Generator
    {
        $email = 'test@test.com';
        $password = '12345678';
        $user = User::withCredentials($email, $password);
        $task1 = $this->mockTask($user->getId());
        $task2 = $this->mockTask($user->getId());

        yield 'find tasks for user and date' => [
            'user' => $user,
            'mock' => fn () => $this->mock(TaskRepository::class)
                    ->method('getAllByUserIdAndDate')
                    ->willReturn([$task1, $task2]),
            'status' => StatusCodeInterface::STATUS_OK,
            'expected' => new TaskResponse([$task1, $task2]),
        ];
    }

    /**
     * @dataProvider readTestDataProvider
     * @param array<mixed> $expected
     */
    public function testRead(User $user, Task $task, Closure $mock, int $status, array $expected): void
    {
        // GIVEN
        $mock->call($this);

        // WHEN
        $request = $this->createJsonRequest('GET', sprintf('/api/v1/task/%s', $task->getId()->asScalar()));
        $request = $request->withAttribute('token', ['id' => $user->getId()->asScalar()]);
        $response = $this->app->handle($request);

        // THEN
        self::assertEquals($status, $response->getStatusCode());
        $this->assertJsonData($expected, $response);
    }

    /**
     * @return Generator<array>
     */
    public function readTestDataProvider(): Generator
    {
        $email = 'test@test.com';
        $password = '12345678';
        $user = User::withCredentials($email, $password);
        $task = $this->mockTask($user->getId());

        yield 'find tasks for user and date' => [
            'user' => $user,
            'task' => $task,
            'mock' => fn () => $this->mock(TaskRepository::class)
                ->method('getByTaskAndUserId')
                ->willReturn($task),
            'status' => StatusCodeInterface::STATUS_OK,
            'expected' => TaskTransformer::transformOne($task)->toArray(),
        ];
    }

    private function mockTask(UserId $userId): Task
    {
        return Task::new(
            $userId,
            new DateTime('now'),
            'test'
        );
    }
}
